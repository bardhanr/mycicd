variable "myregion" {
type = string
description = "Your AWS Region Name"
}
variable "secret-key" {
sensitive = true
}
variable "access-key"{
sensitive = true
}
variable "vpcname" {
default = "ran-myvpc1"
}
variable "aminame" {
type = map
default = {
us-west-2 = "ami-019d533b7b2201eff"
us-east-1 = "ami-052efd3df9dad4825"
ap-northeast-1 = "ami-03f4fa076d2981b45" ##ubuntuServer22.04 0n tokyo
}
}

